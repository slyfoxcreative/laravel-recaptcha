<?php

declare(strict_types=1);

use function SlyFoxCreative\Utilities\assert_string;

function recaptcha_script(): string
{
    return '<script defer src="https://www.google.com/recaptcha/api.js"></script>';
}

function recaptcha_tag(): string
{
    $siteKey = config('recaptcha.site_key');
    assert_string($siteKey);

    return "<div class='g-recaptcha' data-sitekey='{$siteKey}'></div>";
}
