<?php

declare(strict_types=1);

namespace SlyFoxCreative\ReCaptcha;

use Illuminate\Http\Request;
use ReCaptcha\ReCaptcha;
use Symfony\Component\HttpFoundation\Response;

use function SlyFoxCreative\Utilities\assert_string;

class CheckReCaptcha
{
    public function handle(Request $request, \Closure $next): Response
    {
        $secretKey = config('recaptcha.secret_key');
        assert_string($secretKey);
        $recaptcha = new ReCaptcha($secretKey);

        $response = $request->input('g-recaptcha-response');
        assert_string($response);

        if (! $recaptcha->verify($response)->isSuccess()) {
            return back()
                ->with('error', 'Please verify that you are not a robot.')
            ;
        }

        return $next($request);
    }
}
