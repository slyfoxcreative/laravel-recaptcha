<?php

declare(strict_types=1);

namespace SlyFoxCreative\ReCaptcha\Tests;

use Orchestra\Testbench\TestCase;

class HelpersTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        config(['recaptcha.site_key' => 'TESTKEY']);
    }

    public function testReCaptchaScript(): void
    {
        self::assertSame(
            '<script defer src="https://www.google.com/recaptcha/api.js"></script>',
            recaptcha_script(),
        );
    }

    public function testReCaptchaTag(): void
    {
        self::assertSame(
            "<div class='g-recaptcha' data-sitekey='TESTKEY'></div>",
            recaptcha_tag(),
        );
    }
}
